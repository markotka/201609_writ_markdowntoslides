---
title       	: Lambda Expressions
subtitle    	: Advanced Programming 2016/17, Lecture 12
author      	: Marko Tkal\v{c}i\v{c}
institute		: Free University of Bolzano
logo        	: unibz-logo.png
mode        	: selfcontained # {standalone, draft}
theme			: metropolis
footer 			: Marko Tkal\v{c}i\v{c}, Lambda Expressions
shownotes		: true
section-titles	: false
---

# Introduction

## First slide

- prvi
- drugi
- tretji

## Verbatim text


    some text that should be verbatim { askjd }, indented by a tab or four spaces

    package test;
		public class First {
			public static void main(String[] args) {
				// TODO Auto-generated method stub
		}
	}



## Fenced code

~~~
package test;
		public class First {
			public static void main(String[] args) {
				// TODO Auto-generated method stub
		}
	}
~~~


## Code 

Some code

```java
public static void function something(int a, String b){
	String c = b;
	return c;
}
```

and some other code

```java
package test;

public class First {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
```


## Incremental code

>- ```java
public static void function something(int a, String b){
	String c = b;
	return c;
}```
>- ```java
public static void function something(int a, String b){
	String c = b;
	return c;
}```


#Conclusion

## Line Blocks

| The limerick packs laughs anatomical
| In space that is quite economical.
|    But the good ones I've seen
|    So seldom are clean
| And the clean ones so seldom are comical

| 200 Main St.
| Berkeley, CA 94718


## Incremental bulltes

should be something incremental

>- first
>- second
>- third

## Nested lists

* fruits
    + apples
        - macintosh
        - red delicious
    + pears
    + peaches
* vegetables
    + broccoli
    + chard

## A slide with math

Some math

$$ \alpha = { 1 \over a + b} $$

## Tables

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3


| user-id | item-id | rating |
|--------:|---------|:-------|
|17|23|4|
|44|2342|5|

***

a new slide after three after stars (without title) 


***


## Some figures

![Clusters in the BFI space](clusters.pdf "Voyage to the moon"){width=10cm}
![Clusters in the BFI space](clusters.pdf "Voyage to the moon"){width=50%}

## A slide with blocks

### Block title

The block is done using the header number 3



## Last Slide

skdjfhskjdh


