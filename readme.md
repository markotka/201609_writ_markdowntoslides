# Markdown to PDF slides

A makefile for generating study material (PDF slides, handouts, HTML slides, LaTeX source) from Markdown files. 

I modified the Modern Beamer template (https://bloerg.net/2014/09/20/a-modern-beamer-theme.html) to accomodate some specific requirements (e.g. smaller fonts for code embedding).

The ```demo.md``` file showcases the functionalities.

## Acknowledgement

I used code or was inspired by

- http://jeromyanglim.blogspot.it/2012/07/beamer-pandoc-markdown.html
- https://github.com/jgm/pandoc-templates
- https://bloerg.net/2014/09/20/a-modern-beamer-theme.html
- http://benschmidt.org/2014/11/07/building-outlines-for-markdown-documents-with-pandoc/
- http://benschmidt.org/2014/09/05/markdown-historical-writing-and-killer-apps/
- https://andrewgoldstone.com/blog/2014/12/24/slides/
- http://pandoc.org/demo/example13.pdf
- http://rmarkdown.rstudio.com/articles_beamer.html
- https://gist.github.com/lmullen/c3d4c7883f081ed8692a
- http://www.bcaffo.com/

## License

[![Foo](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/) This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/) .



