

ALL_SOURCE_DOCS := $(wildcard *.md)

# .md files that should not be compiled
EXCEPTIONS= readme.md

# .md files to be compiled
SOURCE_DOCS := $(filter-out $(EXCEPTIONS),$(ALL_SOURCE_DOCS))


EXPORTED_DOCS=\
 $(SOURCE_DOCS:.md=.slides.pdf) \
 $(SOURCE_DOCS:.md=.handouts.pdf) \
 $(SOURCE_DOCS:.md=.html) \
 $(SOURCE_DOCS:.md=.tex) \

# locations of binaries
RM=/bin/rm 
PANDOC=/usr/local/bin/pandoc

# pandoc options
PANDOC_BEAMER_OPTIONS=--slide-level 2 -t beamer --template=default.beamer.markot.tex
PANDOC_LATEX_OPTIONS=--slide-level 2 -s -t beamer --template=default.beamer.markot.tex
PANDOC_HTML_OPTIONS=-t slidy -s
PANDOC_HANDOUT_OPTIONS=

# generate PDF slides
%.slides.pdf: %.md
	$(PANDOC) $< $(PANDOC_BEAMER_OPTIONS) -o $@ 

# generate PDF handouts
%.handouts.pdf: %.md
	$(PANDOC) $< $(PANDOC_HANDOUT_OPTIONS) -o $@ 

# generate beamer LaTeX file for manual correction
%.tex: %.md
	$(PANDOC) $< $(PANDOC_LATEX_OPTIONS) -o $@ 

# generate HTML slides
%.html: %.md
	$(PANDOC) $< $(PANDOC_HTML_OPTIONS) -o $@ 


.PHONY: all clean

clean:
	$(RM) $(EXPORTED_DOCS)


all: $(EXPORTED_DOCS)


#	pandoc test.md --slide-level 2 -t beamer --template=default.beamer.markot.tex -o test.pdf
#	pandoc test.md --slide-level 2 -s -t beamer --template=default.beamer.markot.tex -V theme:metropolis -o test.tex
#	pandoc test.md -t slidy -s  -o test.html

